<?php
/** 
 * Expose our views plugins.
 */
function yagm_views_style_plugins() {
  return array(
    'yagm' => array(
      'name' => t('Image Gallery'),
      'theme' => 'yagm_image_gallery',
      'needs_table_header' => true,
      'needs_fields' => true,
      'even_empty' => false,
    )
  );
}

/**
 * Expose our own fields.
 */
function yagm_views_tables() {
  $tables = array(
    'yagm' => array(
      'name' => 'relativity',
      'provider' => 'internal',
      'join' => array(
        'left' => array(
          'table' => 'relativity',
          'field' => 'parent_nid'
        ),
        'right' => array(
          'field' => 'nid'
        )
      ),
      'fields' => array(
        'children_count' => array(
          'name' => t('YAGM: Contents'),
          'handler' => 'views_handler_field_yagm_contents',
          'sortable' => FALSE,
          'nofield' => TRUE,
          'help' => t('Display the count of items in gallery.'),
          'field' => 'nid',
        ),
      ),
    ),
  );
  return $tables;
}

function views_handler_field_yagm_contents($fieldinfo, $fielddata, $value, $data) {
  $image_count = db_result(db_query('SELECT COUNT(r.nid) AS c FROM {relativity} r INNER JOIN {node} n ON r.nid = n.nid WHERE n.type = "image" AND r.parent_nid = %d', $data->nid));
  $image_string = format_plural($image_count, t('image'), t('images'));

  $gallery_count = db_result(db_query('SELECT COUNT(r.nid) AS c FROM {relativity} r INNER JOIN {node} n ON r.nid = n.nid WHERE n.type = "gallery" AND r.parent_nid = %d', $data->nid));
  $gallery_string = format_plural($gallery_count, t('gallery'), t('galleries'));
  return t('%gcount @gstring and %icount @istring', array('%icount' => $image_count, '%gcount' => $gallery_count, '@istring' => $image_string, '@gstring' => $gallery_string));
}


/** 
 * Theme a YAGM view type.
 * 
 * @param $view The view object.
 * @param $nodes A multidimension array of nodes.
 * @param $type View type.
 * 
 * @return Rendered HTML prepresentaion of the requested view.
 */
function theme_yagm_image_gallery($view, &$nodes, $type) {
  // Add our stylesheet
  drupal_add_css(drupal_get_path('module', 'yagm') .'/yagm.css');
  // Theme the view
  $fields = _views_get_fields();
  $header = array('', '');
  foreach ($nodes as $node) {
    $items = array();
    $preview = null;
    foreach ($view->field as $field) {
      $item = $item_label = $item_value = null;
      if (!isset($fields[$field['id']]['visible']) && $fields[$field['id']]['visible'] !== FALSE) {
        if ($field['label']) {
          $item_label = '<span class="view-label '. views_css_safe('view-label-'. $field['queryname']) .'">'. $field['label'] .': </span>';
        }
        $value = views_theme_field('views_handle_field', $field['queryname'], $fields, $field, $node, $view);
        $item_value = '<span class="view-field '. views_css_safe('view-data-'. $field['queryname']) .'">'. $value .'</span>';
      }
      if ($node->yagm_type == 'gallery') {
        // If no preview found, i.e. the node is a gallery not an image:
        $children = views_get_view('gallery_images');
        $children->page_empty = t('No preview available');
        // Send the parent ID as the view's first argument:
        $children_args = array($node->nid);
        // Get the view output, i.e. build the view
        $first_child = views_build_view('embed', $children, $children_args, 0, 1);
        $preview = array(
          'data' => $first_child,
          'width' => '100px',
        );
      }
      if ($node->yagm_type == 'image' && $field['queryname'] == 'yagm_children_count') {
        // In case of an image, skip the children count field
        continue;
      }
      if ($field['queryname'] == 'node_data_field_image_field_image_fid' && $node->yagm_type == 'image') {
        // In case of an image, show the image field as the preview
        $preview = array(
          'data' => $item_value,
          'width' => '100px',
        );
      }
      else {
        $items[] = $item_label . $item_value;
      }
    }
    $rows[] = array($preview, theme('item_list', $items));
  }
  $output .= theme('table', $header, $rows);
  return $output;
}

/** 
 * Implementation of hook_views_query_alter().
 * Load some extra fields in our views.
 */
function yagm_views_query_alter(&$query, &$view, $summary, $level) {
  if ($view->page_type == 'yagm') {
    $query->fields[] = 'node.type AS yagm_type';
  }
}

/** 
 * Expose YAGM default views.
 * 
 * @return A multidimenstion array of views to expose.
 */
function yagm_views_default_views() {
  $view = new stdClass();
  $view->name = 'user_galleries';
  $view->disabled = false;
  $view->description = t('A list of galleries per user.');
  $view->access = array();
  $view->page = true;
  $view->page_title = t('Galleries');
  $view->page_header = t('This is a list of your galleries.');
  $view->page_header_format = '1';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'No galleries found. '. l('Create one?', 'node/add/gallery');
  $view->page_empty_format = '1';
  $view->page_type = 'yagm';
  $view->url = 'user/$arg/gallery';
  $view->use_pager = true;
  $view->nodes_per_page = '10';
  $view->menu = TRUE;
  $view->menu_title = t('Galleries');
  $view->menu_tab = TRUE;
  $view->menu_tab_weight = '10';
  $view->menu_tab_default = FALSE;
  $view->menu_tab_default_parent = NULL;
  $view->menu_tab_default_parent_type = 'tab';
  $view->menu_parent_tab_weight = '0';
  $view->menu_parent_title = '';
  $view->sort = array(
    array(
      'tablename' => 'node',
      'field' => 'sticky',
      'sortorder' => 'DESC',
      'options' => '',
    ),
    array(
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'DESC',
      'options' => '',
    ),
  );
  $view->argument = array(
    array(
      'type' => 'uid',
      'argdefault' => '1',
      'title' => '%1',
      'options' => '0',
    ),
  );
  $view->field = array(
    array(
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Title',
      'handler' => 'views_handler_field_nodelink_with_mark',
      'options' => 'link',
    ),
    array(
      'tablename' => 'node',
      'field' => 'changed',
      'label' => 'Updated',
      'handler' => 'views_handler_field_date_small',
    ),
    array (
      'tablename' => 'yagm',
      'field' => 'children_count',
      'label' => 'Contents',
    ),
  );
  $view->filter = array(
    array(
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array(
      'tablename' => 'node',
      'field' => 'type',
      'operator' => '=',
      'options' => '',
      'value' => 'gallery',
    ),
  );
  $view->requires = array(node, users);
  $views[$view->name] = $view;

  // Gallery children view
  $view = new stdClass();
  $view->name = 'gallery_children';
  $view->disabled = false;
  $view->description = t('A list of images or galleries or both.');
  $view->access = array();
  $view->page = true;
  $view->page_title = t('Galleries');
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'Neither images nor galleries found.';
  $view->page_empty_format = '1';
  $view->page_type = 'yagm';
  $view->url = 'gallery';
  $view->use_pager = true;
  $view->nodes_per_page = '5';
  $view->menu = FALSE;
  $view->menu_title = 'Galleries';
  $view->sort = array(
    array(
      'tablename' => 'node',
      'field' => 'sticky',
      'sortorder' => 'DESC',
      'options' => '',
    ),
    array(
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'DESC',
      'options' => '',
    ),
  );
  $view->argument = array(
    array (
      'type' => 'relativity_parent',
      'argdefault' => '2',
      'title' => '',
      'options' => '0',
    ),
  );
  $view->field = array(
    array(
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Title',
      'handler' => 'views_handler_field_nodelink_with_mark',
      'options' => 'link',
    ),
    array(
      'tablename' => 'node',
      'field' => 'changed',
      'label' => 'Updated',
      'handler' => 'views_handler_field_date_small',
    ),
    array(
      'tablename' => 'users',
      'field' => 'name',
      'label' => 'Author',
    ),
    array (
      'tablename' => 'node_data_field_image',
      'field' => 'field_image_fid',
      'label' => 'Image',
      'handler' => 'content_views_field_handler_group',
      'options' => 'yagm_small_linked',
    ),
    array (
      'tablename' => 'yagm',
      'field' => 'children_count',
      'label' => 'Contents',
    ),
  );
  $view->filter = array(
    array(
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array(
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array(
        0 => 'gallery',
        1 => 'image',
      ),
    ),
  );
  $view->requires = array(node, users);
  $views[$view->name] = $view;

  // Gallery images
  $view = new stdClass();
  $view->name = 'gallery_images';
  $view->disabled = false;
  $view->description = t('A list of images in a gallery.');
  $view->access = array();
  $view->page = true;
  $view->page_title = t('Images');
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'No images found.';
  $view->page_empty_format = '1';
  $view->page_type = 'list';
  $view->url = 'images';
  $view->use_pager = true;
  $view->nodes_per_page = '5';
  $view->sort = array (
    array (
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'DESC',
      'options' => 'normal',
    ),
  );
  $view->argument = array (
    array (
      'type' => 'relativity_parent',
      'argdefault' => '2',
      'title' => '',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'node_data_field_image',
      'field' => 'field_image_fid',
      'label' => '',
      'handler' => 'content_views_field_handler_group',
      'options' => 'yagm_small_linked',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array('image'),
    ),
  );
  $view->requires = array(node, node_data_field_image);
  $views[$view->name] = $view;

  return $views;
}
?>
